#include <iostream>
#include "ToyMemoryManager.h"

int main(int argc, const char * argv[]) {
    ToyMemoryManager manager = ToyMemoryManager(10);
    cout << manager.malloc(2) << "\n";
    cout << manager.malloc(2) << "\n";
    cout << manager.malloc(2) << "\n";
    cout << manager.free(1) << "\n";
    cout << manager.malloc(4) << "\n";
    cout << manager.free(0) << "\n";
    cout << manager.malloc(7) << "\n";
}
