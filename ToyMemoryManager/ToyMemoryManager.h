#include <stack>
#include <list>
#include <vector>

using namespace::std;

enum SegmentStatus {
    IS_FREE,
    IS_ALLOCATED,
    IS_DELETED,
};

struct MemorySegment {
    SegmentStatus status;
    int length;
};

class ToyMemoryManager {
private:
    list<MemorySegment *> segmentList;
    stack<MemorySegment *> freeSegments;
    vector<MemorySegment *> memoryCells;

public:
    ToyMemoryManager(int capacity) {
        MemorySegment *initialSegment = new MemorySegment({IS_FREE, capacity});

        this->segmentList = list<MemorySegment *>{initialSegment};

        this->freeSegments = stack<MemorySegment *>();
        freeSegments.push(initialSegment);

        this->memoryCells = vector<MemorySegment *>(capacity);
    }

    int malloc(int n) {
        while (freeSegments.top()->status == IS_DELETED) {
            delete freeSegments.top();
            freeSegments.pop();
        }

        if (freeSegments.top()->length < n) {
            return -1;
        }

        MemorySegment *freeSegment = freeSegments.top();
        freeSegments.pop();

        for (auto it = segmentList.begin(); it != segmentList.end(); ) {
            if (*it == freeSegment) {
                it = segmentList.erase(it);
                MemorySegment *allocatedSegment = new MemorySegment({IS_ALLOCATED, n});
                segmentList.insert(it, allocatedSegment);

                int freeLength = freeSegment->length - n;
                delete freeSegment;
                if (freeLength > 0) {
                    MemorySegment *newFreeSegment = new MemorySegment({IS_FREE, freeLength});
                    ++it;
                    segmentList.insert(it, newFreeSegment);
                    freeSegments.push(newFreeSegment);
                }

                for (int i = 0; i <= memoryCells.size() - 1; i++) {
                    if (memoryCells[i] == nullptr) {
                        memoryCells[i] = allocatedSegment;
                        return i;
                    }
                }
            } else {
                ++it;
            }
        }

        return -1;
    }

    int free(int i) {
        if (memoryCells[i] == nullptr) {
            return -1;
        }

        MemorySegment *allocatedSegment = memoryCells[i];
        memoryCells[i] = nullptr;

        for (auto it = segmentList.begin(); it != segmentList.end(); ) {
            if (*it == allocatedSegment) {
                segmentList.erase(it);

                int freeTotalLength = allocatedSegment->length;

                // previous
                --it;
                if ((*it)->status == IS_FREE) {
                    freeTotalLength += (*it)->length;
                    (*it)->status = IS_DELETED;
                    segmentList.erase(it);
                }

                // next
                ++it;
                ++it;
                if ((*it)->status == IS_FREE) {
                    freeTotalLength += (*it)->length;
                    (*it)->status = IS_DELETED;
                    segmentList.erase(it);
                }

                // to initial position
                --it;

                MemorySegment *newFreeSegment = new MemorySegment({IS_FREE, freeTotalLength});
                segmentList.insert(it, newFreeSegment);

                freeSegments.push(newFreeSegment);
                return 0;
            } else {
                ++it;
            }
        }

        return -1;
    }
};
